Forked from [GitHub](https://github.com/H-man/UnityVertexColors.git)
-

UnityVertexColours
===
Unity Standard shader with vertex colours originally found from: https://forum.unity3d.com/threads/standard-shader-with-vertex-colors.316529/ converted to Unity 2017.2.0f3

![alt tag](https://i.imgur.com/FBz5EaT.jpg)
